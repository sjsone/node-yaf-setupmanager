#!/usr/bin/env node

import NodeFs from "fs";
import NodePath from "path";

import { exec } from 'child_process'

const assert = (value: any, message: string, exitCode = 1) => {
    if (!value) {
        console.error('Error: ', message);
        process.exit(exitCode);
    }
};

const commandArgs = process.argv.splice(2);
const projectName = commandArgs[0];

assert(projectName, "Invalid ProjectName");

const basePath = NodePath.join(process.cwd(), projectName);

const paths = [
    basePath,
    NodePath.join(basePath, "out"),
    NodePath.join(basePath, "src")
]
for (const path of paths) {
    assert(!NodeFs.existsSync(path), `Path exists: "${path}"`)
    NodeFs.mkdirSync(path, { recursive: true });
}

const baseTsConfig = {
    compilerOptions: {
        target: "ES2019",
        module: "commonjs",
        declaration: true,
        outDir: "./out",
        rootDir: "./src",
        strict: true,
        esModuleInterop: true,
        forceConsistentCasingInFileNames: true,
    },
};

const basePackageJson = {
    name: projectName.replace('node-', ''),
    version: '0.0.1',
    description: "PlaceHolder",
    dependencies: {},
    devDependencies: {},
    scripts: {
        test: "echo \"Error: no test specified\" && exit 1",
        prepublishOnly: "tsc",
        'watch-ts': "tsc --watch",
        'watch-src': "nodemon out/main.js"
    },
    main: "out/main.js",
    author: "",
    license: "ISC"
}

NodeFs.writeFileSync(NodePath.join(basePath, 'tsconfig.json'), JSON.stringify(baseTsConfig, undefined, 4))
NodeFs.writeFileSync(NodePath.join(basePath, 'package.json'), JSON.stringify(basePackageJson, undefined, 4))
NodeFs.writeFileSync(NodePath.join(basePath, 'src', 'main.ts'), '')

NodeFs.writeFileSync(NodePath.join(basePath, '.gitignore'), [
    'node_modules/',
    'out/',
    '.DS_Store'
].join('\n'))

NodeFs.writeFileSync(NodePath.join(basePath, '.npmignore'), [
    'src/',
    '.DS_Store'
].join('\n'))


const commands = [
    'yarn add --save-dev @types/node typescript',
    'git init'
]

for (const command of commands) {
    exec(command, { cwd: basePath }, (error, stdout, stderr) => {
        console.log(stdout)
        console.log(stderr)
    })
}
